import { Component } from '.';

export class Moving implements Component {
    public to: { x: number; y: number };
    constructor(x: number, y: number) {
        this.to = {
            x,
            y,
        };
    }
}
