import { Component } from '.';

/**
 * Has a position in space
 * All items are assumed to take up a single square (for now)
 */
export class Position implements Component {
    public x: number;
    public y: number;
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}
