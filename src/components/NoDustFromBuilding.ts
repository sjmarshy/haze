import { Component } from '.';

/**
 * when a building banishes dust, we need to know which one
 */
export class NoDustFromBuilding implements Component {
    public building: string;
    constructor(building: string) {
        this.building = building;
    }
}
