import { Component } from '.';

/**
 * something that exists on a tile
 */

export class OnTile implements Component {
    public tile: string;
    constructor(tile: string) {
        this.tile = tile;
    }
}
