export interface Component {}
export type ComponentType<T extends Component = Component> = new (...any) => T;

// Marker Components
export * from './KeyboardControlled';
export * from './Corporeal';
export * from './Died';
export * from './DustIncreased';

// Data Components
export * from './Attacking';
export * from './Vulnerable';
export * from './Armed';
export * from './Position';
export * from './Sprite';
export * from './Moving';
export * from './Dusted';
export * from './DustInventory';
