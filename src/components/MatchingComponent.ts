import { IEntity } from '../Entity';
import { Component } from './index';
import { Option, some, none } from 'fp-ts/lib/Option';

export class MatchingComponent<T extends Component> {
    private entity: IEntity;
    private match: new (...any) => T;
    constructor(entity: IEntity, match: new (...any) => T) {
        this.entity = entity;
        this.match = match;
    }
    public component(): Option<T> {
        const component = this.entity
            .components()
            .filter(c => c instanceof this.match);

        if (component.length >= 1) {
            return some(component[0]) as Option<T>;
        } else {
            return none;
        }
    }
}
