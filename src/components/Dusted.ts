import { Component } from '.';

/**
 * the amount of dust covering something
 * 0 - no dust
 * 1 - completely covered
 */
export class Dusted implements Component {
    public amount: number;
    constructor(amount: number) {
        this.amount = Math.max(0, Math.min(1, amount));
    }
}
