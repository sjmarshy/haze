import { Component } from '.';

/**
 * Something which can cause damage
 */
export class Armed implements Component {
    public damage: number;
    constructor(damage: number) {
        this.damage = damage;
    }
}
