import { Component } from '.';

export type Mask = { x: number; y: number; w: number; h: number };

export class Sprite implements Component {
    public textureFile: string;
    public mask: Mask;
    public transparency: number;
    constructor(textureFile: string, mask: Mask, transparency: number = 1) {
        this.textureFile = textureFile;
        this.mask = mask;
        this.transparency = Math.max(0, Math.min(1, transparency));
    }
}
