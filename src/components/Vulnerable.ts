import { Component } from './index';

/**
 * Something which can be damaged
 * Currently assumes that vulnerable things will *die*, but this may not always be the case
 *   - some things may die, others may become broken
 */
export class Vulnerable implements Component {
    public hp: number;
    public maxHP: number;
    constructor(startingHP: number, maxHP: number = Infinity) {
        this.hp = startingHP;
        this.maxHP = maxHP;
    }
}
