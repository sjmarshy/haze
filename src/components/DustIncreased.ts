import { Component } from '.';

/**
 * Marks an entity as having grown this turn
 */
export class DustIncreased implements Component {}
