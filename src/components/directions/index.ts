import { Component } from '..';

export class East implements Component {
    public tile: string;
    constructor(tile: string) {
        this.tile = tile;
    }
}
export class West implements Component {
    public tile: string;
    constructor(tile: string) {
        this.tile = tile;
    }
}
export class North implements Component {
    public tile: string;
    constructor(tile: string) {
        this.tile = tile;
    }
}
export class South implements Component {
    public tile: string;
    constructor(tile: string) {
        this.tile = tile;
    }
}
