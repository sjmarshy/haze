import { Component } from '.';

/**
 * allows an entity to 'carry' dust
 */
export class DustInventory implements Component {
    /**
     * the amount of dust held
     */
    public carried: number;
    constructor(startingDust: number = 0) {
        this.carried = startingDust;
    }
}
