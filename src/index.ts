import tileset from '../assets/16x16_tileset.bmp';
import { Container, Application, loader } from 'pixi.js';

import {
    Position,
    Armed,
    Vulnerable,
    Corporeal,
    Sprite,
    KeyboardControlled,
    Dusted,
    DustInventory,
} from './components';
import { Entity, IEntityGroup, EntityGroup, IEntity } from './entity';

import { Keypress } from './events';
// TODO: add index file to systems.
import { RenderSystem } from './systems/RenderSystem';
import { MovementSystem } from './systems/MovementSystem';
import { DustSystem } from './systems/DustSystem';
import { CombatSystem } from './systems/CombatSystem';
import { KeyboardControlSystem } from './systems/KeyboardControlSystem';
import { East, West, South, North } from './components/directions';
import { BuildingSystem } from './systems/BuildingSystem';

class Game {
    private display: Container;
    private entities: IEntityGroup;
    constructor() {
        this.entities = new EntityGroup([]);
        this.init();
    }

    private init() {
        const pixiApp = new Application({
            width: 512 + 256 + 128,
            height: 512,
        });
        document.body.appendChild(pixiApp.view);
        this.display = pixiApp.stage;

        loader.add(tileset).load(() => {
            const cols = pixiApp.screen.width / 16;
            const rows = pixiApp.screen.height / 16;

            /**
             * TODO: efficient 'next to' - probably in the form of components
             *    North
             *    South
             *    East
             *    West
             *
             * this would also make 'can I go this way' stuff easier
             */
            const mapTiles = new Array(cols * rows).fill(undefined).map(
                (_, i) =>
                    new Entity(
                        // TODO: add floortile component
                        new Sprite(tileset, {
                            x: 5 * 16,
                            y: 0 * 16,
                            w: 16,
                            h: 16,
                        }),
                        new Position(i % cols, Math.floor(i / cols)),
                        new Dusted(Math.random()),
                    ),
            );
            mapTiles.forEach((curr, i, arr) => {
                const west = i >= 1 ? mapTiles[i - 1] : null;
                const north = i > cols ? mapTiles[i - cols] : null;
                if (west != null) {
                    west.addComponent(new East(curr.uuid));
                    curr.addComponent(new West(west.uuid));
                }
                if (north != null) {
                    north.addComponent(new South(curr.uuid));
                    curr.addComponent(new North(north.uuid));
                }
            });

            const player = new Entity(
                new KeyboardControlled(),
                new Corporeal(),
                new Position(0, 0),
                new Sprite(tileset, {
                    x: 0 * 16,
                    y: 18 * 16,
                    w: 16,
                    h: 16,
                }),
                new Armed(5),
                new DustInventory(0),
            );

            const monster = new Entity(
                new Corporeal(),
                new Position(10, 10),
                new Sprite(tileset, {
                    x: 7 * 16,
                    y: 18 * 16,
                    w: 16,
                    h: 16,
                }),
                new Vulnerable(10),
            );

            this.entities = this.entities.add([...mapTiles, monster, player]);

            const keyboardSystem = new KeyboardControlSystem();
            const movementSystem = new MovementSystem();
            const combatSystem = new CombatSystem();
            const dustSystem = new DustSystem();
            const renderSystem = new RenderSystem(this.display);
            const buildingSystem = new BuildingSystem();

            const tick = (event?: KeyboardEvent) => {
                if (event) {
                    keyboardSystem.run(
                        new Keypress(event.keyCode),
                        this.entities,
                    );
                }
                buildingSystem.run(this.entities);
                movementSystem.run(this.entities);
                combatSystem.run(this.entities);
                dustSystem.run(this.entities);
                renderSystem.run(this.entities);
            };

            tick();
            // TODO: more sophisticated turn system? at the moment, it's one turn per keypress, which
            //       likely won't scale
            window.onkeyup = event => {
                tick(event);
            };
        });
    }
}

new Game();
