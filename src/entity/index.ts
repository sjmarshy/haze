import uuid from 'uuid/v4';
import { Option, fromNullable } from 'fp-ts/lib/Option';
import { Component, ComponentType } from '../components';
import { IndexedArray } from './IndexedArray';

export class Entity implements IEntity {
    private _components: Array<Component> = [];
    private _uuid: string;
    constructor(...components: Array<Component>) {
        this._uuid = uuid();
        this._components = components;
    }
    public get uuid() {
        return this._uuid;
    }
    public addComponent(c) {
        this._components.push(c);
    }
    public components() {
        return this._components;
    }
    /**
     * does this entity contain a specific component?
     */
    public has(test: new (...els: any) => Component) {
        return this._components.some(component => component instanceof test);
    }
    public hasAll(cs: Array<ComponentType>) {
        return cs.every(c => this.has(c));
    }
    /**
     * remove a component
     */
    public remove(test: new (...els: any) => Component) {
        this._components = this._components.filter(
            component => !(component instanceof test),
        );
    }
    public get<T extends Component>(test: ComponentType<T>): Option<T> {
        return fromNullable(this._components.find(c => c instanceof test) as T);
    }
}

export interface IEntity {
    // each entity has a UUID
    uuid: string;
    addComponent(c: Component): void;
    components(): Array<Component>;
    has(c: new (...any) => Component): boolean;
    /**
     * entity contains all the provided components
     */
    hasAll(cs: Array<ComponentType>): boolean;
    /**
     * get the associated component if it exists
     */
    get<T extends Component>(c: ComponentType<T>): Option<T>;
    remove(c: new (...any) => Component): void;
}

/**
 * the plan is to use this to pass in systems. Each 'screen' can have a different entity group, to keep the pool small
 */
export interface IEntityGroup {
    getByUUID(uuid: string): Option<IEntity>;
    /**
     * filter the entity list down
     */
    filter(predicate: (e: IEntity) => boolean): IEntityGroup;
    /**
     * call a fn for each entity in the group
     */
    forEach(fn: (e: IEntity) => void): void;
    add(e: Array<IEntity>): IEntityGroup;

    mutAdd(e: Array<IEntity>): void;
    mutRemove(e: IEntity): void;
}

export class EntityGroup implements IEntityGroup {
    private _entities: Array<IEntity>;
    private _uuidIndexedEntities: IndexedArray<IEntity, 'uuid'>;
    constructor(entities: Array<IEntity>) {
        this._entities = entities;
        /**
         * TODO: split this out into private fn
         * TODO: split out concept of `indexed` array to be generic
         */
        this._uuidIndexedEntities = new IndexedArray('uuid', entities);
    }

    public getByUUID(uuid: string): Option<IEntity> {
        return fromNullable(this._uuidIndexedEntities.get(uuid));
    }

    public filter(predicate: (e: IEntity) => boolean) {
        return new EntityGroup(this._entities.filter(predicate));
    }

    public add(es: Array<IEntity>) {
        return new EntityGroup(this._entities.concat(es));
    }

    public mutAdd(es: Array<IEntity>) {
        this._entities.push(...es);
        this._uuidIndexedEntities = new IndexedArray('uuid', this._entities);
    }

    public mutRemove(toRemove: IEntity) {
        const ix = this._entities.findIndex(e => e === toRemove);
        this._entities.splice(ix, 1);
    }

    public forEach(fn: (e: IEntity) => void) {
        this._entities.forEach(fn);
    }
}
