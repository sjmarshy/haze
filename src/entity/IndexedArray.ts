export class IndexedArray<T, U extends keyof T> {
    private indexed: Map<T[U], T>;
    constructor(indexOn: U, arr: Array<T>) {
        this.indexed = arr.reduce((memo: Map<T[U], T>, t) => {
            memo.set(t[indexOn], t);
            return memo;
        }, new Map());
    }

    public get(key: T[U]): T {
        return this.indexed.get(key);
    }
}
