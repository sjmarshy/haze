import { KEYPRESS_EVENT } from './events';

export const RAW_EVENT = 'RAW_EVENT';

export type EVENTS = typeof RAW_EVENT | typeof KEYPRESS_EVENT;

export interface HazeEvent<T = any> {
    readonly type: EVENTS;
    payload(): T;
    match(type: EVENTS, fn: (payload: T) => void): void;
}

export class RawHazeEvent implements HazeEvent {
    readonly type: EVENTS = RAW_EVENT;
    public payload() {
        return {};
    }
    public match(type, fn) {
        this.type === type && fn(this.payload());
    }
}
