import { RawHazeEvent, HazeEvent, EVENTS } from './HazeEvent';
import { KEYPRESS_EVENT } from './events';

export type KeypressPayload = {
    keyCode: number;
};

export class Keypress extends RawHazeEvent
    implements HazeEvent<KeypressPayload> {
    readonly type: EVENTS = KEYPRESS_EVENT;
    private keyCode: number;
    constructor(keyCode: number) {
        super();
        this.keyCode = keyCode;
    }
    public payload() {
        return {
            keyCode: this.keyCode,
        };
    }
}
