import { IEntityGroup } from '../Entity';
import { Keypress } from '../events';
import { MatchingComponent } from '../components/MatchingComponent';
import { KeyboardControlled, Position, Moving } from '../components';
import { BuildAntiDust } from '../components/BuildAntiDust';

export class KeyboardControlSystem {
    constructor() {}

    public run(e: Keypress, entities: IEntityGroup) {
        entities.forEach(mover =>
            new MatchingComponent(mover, KeyboardControlled)
                .component()
                .chain(() => new MatchingComponent(mover, Position).component())
                .map(({ x, y }) => {
                    if (e.payload().keyCode === 72) {
                        // h
                        mover.addComponent(new Moving(x - 1, y));
                    } else if (e.payload().keyCode === 74) {
                        // j
                        mover.addComponent(new Moving(x, y + 1));
                    } else if (e.payload().keyCode === 75) {
                        // k
                        mover.addComponent(new Moving(x, y - 1));
                    } else if (e.payload().keyCode === 76) {
                        // l
                        mover.addComponent(new Moving(x + 1, y));
                    } else if (e.payload().keyCode === 66) {
                        // b
                        mover.addComponent(new BuildAntiDust());
                    }
                }),
        );
    }
}
