import { IEntityGroup } from '../Entity';
import { MatchingComponent } from '../components/MatchingComponent';
import {
    Moving,
    Position,
    Corporeal,
    Vulnerable,
    Armed,
    Attacking,
} from '../components';

export class MovementSystem {
    public run(entities: IEntityGroup) {
        entities.forEach(mover => {
            new MatchingComponent(mover, Moving).component().map(moving => {
                new MatchingComponent(mover, Position)
                    .component()
                    .map(position => {
                        const futurePosition = moving.to;
                        mover.remove(Moving);

                        if (mover.has(Corporeal)) {
                            // then we have to deal with colissions
                            entities
                                .filter(
                                    maybeObstacle =>
                                        maybeObstacle !== mover &&
                                        maybeObstacle.has(Corporeal) &&
                                        maybeObstacle.has(Position),
                                )
                                .filter(obstacle =>
                                    new MatchingComponent(obstacle, Position)
                                        .component()
                                        .fold(
                                            false,
                                            pos =>
                                                futurePosition.x === pos.x &&
                                                futurePosition.y === pos.y,
                                        ),
                                )
                                .forEach(obstacle => {
                                    if (
                                        obstacle.has(Vulnerable) &&
                                        mover.has(Armed)
                                    ) {
                                        mover.addComponent(
                                            new Attacking(obstacle.uuid),
                                        );
                                    }
                                });
                        }
                        position.x = futurePosition.x;
                        position.y = futurePosition.y;
                    });
            });
        });
    }
}
