import { IEntityGroup } from '../Entity';
import { MatchingComponent } from '../components/MatchingComponent';
import {
    Dusted,
    Position,
    Sprite,
    DustInventory,
    ComponentType,
} from '../components';
import { identity } from 'fp-ts/lib/function';
import { North, East, South, West } from '../components/directions';
import { NoDustFromBuilding } from '../components/NoDustFromBuilding';

export class DustSystem {
    public run(entities: IEntityGroup) {
        /**
         * anything with a dust inventory picks up dust
         */
        entities
            .filter(e => e.hasAll([DustInventory, Position]))
            .forEach(dusted => {
                const inv = dusted
                    .get(DustInventory)
                    .fold(new DustInventory(0), identity);
                const pos = dusted
                    .get(Position)
                    .fold(new Position(0, 0), identity);

                entities
                    .filter(e => e.hasAll([Position, Dusted]))
                    .forEach(tile => {
                        const tilePos = tile
                            .get(Position)
                            .fold(new Position(0, 0), identity);

                        if (tilePos.x === pos.x && tilePos.y === pos.y) {
                            const dustedTile = tile
                                .get(Dusted)
                                .fold(new Dusted(0), identity);

                            inv.carried += dustedTile.amount;

                            dustedTile.amount = 0;
                        }
                    });
            });

        /**
         * dust spreads or grows every tick
         */
        entities
            .filter(e => e.has(Dusted))
            .forEach(dusted => {
                const chance = Math.random();

                const updateDustFor = (
                    dir: ComponentType<North | South | East | West>,
                ) =>
                    dusted.get(dir).map(there => {
                        return entities
                            .getByUUID(there.tile)
                            .filter(x => !x.has(NoDustFromBuilding))
                            .map(x => {
                                x.get(Dusted).map(thereDust => {
                                    dusted.get(Dusted).map(hereDust => {
                                        const dustDelta = 0.1 * hereDust.amount;
                                        hereDust.amount -= dustDelta;
                                        thereDust.amount += dustDelta;
                                    });
                                });
                            });
                    });

                if (chance < 0.2) {
                    updateDustFor(North);
                } else if (chance < 0.4) {
                    updateDustFor(East);
                } else if (chance < 0.6) {
                    updateDustFor(South);
                } else if (chance < 0.8) {
                    updateDustFor(West);
                } else if (chance <= 1) {
                    // self
                    dusted.get(Dusted).map(hereDust => {
                        // increase self by 20%
                        if (hereDust.amount < 1) {
                            hereDust.amount += hereDust.amount * 0.05;
                        }
                    });
                }
            });

        /**
         * tile transparency matches dust amount
         * TODO: should this live in the graphics system or some pre-graphics translation layer?
         * TODO: a pre-graphics translation layer to turn things into sprites might be a good move too
         */
        entities.forEach(entity => {
            new MatchingComponent(entity, Dusted).component().map(dust => {
                new MatchingComponent(entity, Sprite)
                    .component()
                    .map(display => {
                        display.transparency = dust.amount;
                    });
            });
        });
    }
}
