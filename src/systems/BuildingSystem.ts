import tileset from '../../assets/16x16_tileset.bmp';
import { IEntityGroup, Entity } from '../Entity';
import { BuildAntiDust } from '../components/BuildAntiDust';
import { Position, Dusted, Sprite } from '../components';
import { North, East, South, West } from '../components/directions';
import { AntiDustBuilding } from '../components/buildings/AntiDustBuilding';
import { OnTile } from '../components/OnTile';
import { NoDustFromBuilding } from '../components/NoDustFromBuilding';

export class BuildingSystem {
    public run(es: IEntityGroup) {
        es.filter(e => e.hasAll([BuildAntiDust, Position])).forEach(
            antiDustBuilder => {
                // find where the builder is
                // build the antiDust building there
                // 'attach' some void dust components to the tiles there
                // VoidDust{ from: buildingUUID };
                antiDustBuilder.get(Position).map(pos => {
                    es.filter(e => {
                        // TODO: tile component!
                        if (e.hasAll([Position, Dusted])) {
                            return e
                                .get(Position)
                                .fold(
                                    false,
                                    p => p.x === pos.x && p.y === pos.y,
                                );
                        } else {
                            return false;
                        }
                    }).forEach(tile => {
                        const building = new Entity(
                            new AntiDustBuilding(),
                            new OnTile(tile.uuid),
                            new Sprite(tileset, {
                                x: 0 * 16,
                                y: 4 * 16,
                                w: 16,
                                h: 16,
                            }),
                        );

                        es.mutAdd([building]);

                        const n = tile
                            .get(North)
                            .chain(x => es.getByUUID(x.tile));

                        const nw = n
                            .chain(x => x.get(West))
                            .chain(x => es.getByUUID(x.tile));
                        const ne = n
                            .chain(x => x.get(East))
                            .chain(x => es.getByUUID(x.tile));

                        const s = tile
                            .get(South)
                            .chain(x => es.getByUUID(x.tile));
                        const sw = s
                            .chain(x => x.get(West))
                            .chain(x => es.getByUUID(x.tile));
                        const se = s
                            .chain(x => x.get(East))
                            .chain(x => es.getByUUID(x.tile));

                        const e = tile
                            .get(East)
                            .chain(x => es.getByUUID(x.tile));
                        const w = tile
                            .get(West)
                            .chain(x => es.getByUUID(x.tile));

                        n.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        ne.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        nw.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        s.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        se.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        sw.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        e.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                        w.map(x => {
                            x.addComponent(
                                new NoDustFromBuilding(building.uuid),
                            );
                            x.get(Dusted).map(d => (d.amount = 0));
                        });
                    });
                });

                antiDustBuilder.remove(BuildAntiDust);
            },
        );
    }
}
