import { Container, Sprite, Rectangle, loader, Text } from 'pixi.js';
import { IEntityGroup } from '../Entity';
import {
    Sprite as HazeSprite,
    Died,
    Position,
    KeyboardControlled,
    DustInventory,
} from '../components';
import { MatchingComponent } from '../components/MatchingComponent';
import { OnTile } from '../components/OnTile';

export class RenderSystem {
    private scene: Container;
    private sprites: Record<string, Sprite> = {};
    private ui: Record<string, Text> = {};
    constructor(display: Container) {
        this.scene = display;
    }

    public run(entities: IEntityGroup) {
        entities.forEach(entity => {
            new MatchingComponent(entity, HazeSprite).component().map(display =>
                new MatchingComponent(entity, Position)
                    .component()
                    .map(position => {
                        if (!this.sprites[entity.uuid]) {
                            const mask = display.mask;
                            const rect = new Rectangle(
                                mask.x,
                                mask.y,
                                mask.w,
                                mask.h,
                            );

                            const texture = loader.resources[
                                display.textureFile
                            ].texture.clone();

                            texture.frame = rect;

                            this.sprites[entity.uuid] = new Sprite(texture);

                            this.scene.addChild(this.sprites[entity.uuid]);
                        }
                        const sprite = this.sprites[entity.uuid];

                        if (entity.has(Died)) {
                            this.scene.removeChild(sprite);
                            entities.mutRemove(entity);
                            return;
                        }

                        sprite.alpha = display.transparency;
                        sprite.x = position.x * 16;
                        sprite.y = position.y * 16;
                    }),
            );
        });

        entities
            .filter(e => e.hasAll([OnTile, HazeSprite]))
            .forEach(entityOnTile => {
                const tile = entityOnTile
                    .get(OnTile)
                    .chain(t => entities.getByUUID(t.tile));


                tile.map(t => {
                    entityOnTile.get(HazeSprite).map(onTileSprite => {
                        if (!this.sprites[entityOnTile.uuid]) {
                            const mask = onTileSprite.mask;
                            const rect = new Rectangle(
                                mask.x,
                                mask.y,
                                mask.w,
                                mask.h,
                            );
                            const texture = loader.resources[
                                onTileSprite.textureFile
                            ].texture.clone();
                            texture.frame = rect;

                            this.sprites[entityOnTile.uuid] = new Sprite(
                                texture,
                            );

                            this.scene.addChild(
                                this.sprites[entityOnTile.uuid],
                            );
                        }

                        const sprite = this.sprites[entityOnTile.uuid];

                        t.get(Position).map(position => {
                            sprite.x = position.x * 16;
                            sprite.y = position.y * 16;
                        });
                    });
                });
            });

        entities
            .filter(e => e.has(KeyboardControlled) && e.has(DustInventory))
            .forEach(player => {
                player.get(DustInventory).map(dust => {
                    if (!this.ui.dustCounter) {
                        this.ui.dustCounter = new Text(
                            `dust: ${dust.carried.toFixed(2)}`,
                            {
                                fill: 'white',
                                fontFamily: 'Helvetica',
                                fontSize: 16,
                            },
                        );
                        this.ui.dustCounter.x = 0;
                        this.ui.dustCounter.y = 0;
                        this.scene.addChild(this.ui.dustCounter);
                    } else {
                        this.ui.dustCounter.text = `dust: ${dust.carried.toFixed(
                            2,
                        )}`;
                    }
                });
            });
    }
}
