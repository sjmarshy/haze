import { IEntityGroup } from '../Entity';
import { MatchingComponent } from '../components/MatchingComponent';
import { Attacking, Armed, Vulnerable, Died } from '../components';

export class CombatSystem {
    public run(entities: IEntityGroup) {
        entities.forEach(attacker => {
            new MatchingComponent(attacker, Attacking)
                .component()
                .map(attack => {
                    const target_ = entities.getByUUID(attack.target);
                    target_.map(target => {
                        new MatchingComponent(attacker, Armed)
                            .component()
                            .map(weapon => {
                                new MatchingComponent(target, Vulnerable)
                                    .component()
                                    .map(life => {
                                        life.hp -= weapon.damage;

                                        if (life.hp <= 0) {
                                            target.addComponent(new Died());
                                        }
                                    });
                            });
                    });
                });
        });
    }
}
